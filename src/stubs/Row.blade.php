<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */
?>
<!--suppress ALL -->

<template>
	<tr>
		@foreach($validations as $key => $validation)
			<td class="text-xs-left">
				{{"{{".$modelNameSingularLowerCase}}.{{$key}} }}
			</td>
		@endforeach
		<td>
			<edit-btn @click="onEdit"/>
		</td>
		<td>
			<remove-btn/>
		</td>

	</tr>
</template>

<script>

	import {types} from "dev/store/modules/{{$modelName}}.module";
	import RemoveBtn from "dev/scaffoldings/RemoveBtn";
	import EditBtn from "dev/scaffoldings/EditBtn";
	import RowMixin from "dev/mixins/RowMixin";

	export default {
		name: "{{$modelNameSingularLowerCase}}-row",
		props: {
			{{$modelNameSingularLowerCase}}:{required:true}
		},
		components: {EditBtn, RemoveBtn},
		mixins: [new RowMixin(types, "{{$modelNameSingularLowerCase}}")],
		data() {
			return {};
		},
		computed: {},
		methods: {
			doRemove() {
				this.$store.dispatch(types.REMOVE, this.{{$modelNameSingularLowerCase}}).then(() => {
				});
			},
			onEdit() {
				this.$emit("edit", this.{{$modelNameSingularLowerCase}});
			}
		}
	};
</script>

<style>


</style>