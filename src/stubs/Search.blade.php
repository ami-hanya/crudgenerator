<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/06/18
 * Time: 12:13
 */

/**

 **/
use Closeapp\CrudGenerator\helper\StringManipulation;

$sm = StringManipulation::create($modelName);
?>
namespace App\Repositories\{{$sm->getModelNamePlural()}};


use Closeapp\Repository\Search;

class {{$modelName}}Search extends Search
{


}
