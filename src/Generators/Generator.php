<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/07/18
 * Time: 08:50
 */

namespace Closeapp\CrudGenerator\Generators;


use View;

abstract class Generator
{

	abstract public function create(): string;

	abstract public function getPath(): string;

	protected static function getStub($type, $data = [], $addPHP = true)
	{
		return ($addPHP ? "<?php" . PHP_EOL : "") . View::file(__DIR__ . "\..\\stubs\\$type.blade.php",
				$data)->render();
	}
}