<?php
/**
 * Created by PhpStorm.
 * User: elad
 * Date: 11/07/18
 * Time: 08:53
 */

namespace Closeapp\CrudGenerator\helper;


use Illuminate\Database\Eloquent\Model;

class DataManager
{
	/**
	 * @var string
	 */
	private $modelName;
	private $model;
	private $options;


	/**
	 * DataManager constructor.
	 * @param string $modelName
	 */
	public function __construct($modelName)
	{
		$this->modelName = $modelName;
		$this->model = self::getModelClass($modelName);
		$this->options = $this->getNameOptions($modelName);
	}

	/**
	 * @return mixed
	 */
	public function getOptions()
	{
		return $this->options;
	}


	/**
	 * @return mixed
	 */
	public function getModel()
	{
		return $this->model;
	}

	/**
	 * @return string
	 */
	public function getModelName(): string
	{
		return $this->modelName;
	}

	public function modelNamePlural()
	{
		return str_plural($this->modelName);
	}

	public function modelNamePluralLowerCase()
	{
		return strtolower($this->modelNamePlural());
	}

	public function modelNameSingularLowerCase()
	{
		return strtolower($this->modelName);
	}

	private function getNameOptions(string $name): array
	{
		return [
			"modelName" => $name,
			"modelNamePlural" => str_plural($name),
			"modelNamePluralLowerCase" => strtolower(str_plural($name)),
			"modelNameSingularLowerCase" => strtolower($name)
		];
	}

	/**
	 * @param $modelName
	 * @return Model
	 */
	public static function getModelClass($modelName): Model
	{
		$modelClassName = "\App\\" . $modelName;
		return new $modelClassName;
	}
}